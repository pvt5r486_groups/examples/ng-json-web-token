import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    private _jwtToken: string;
    private _urlPrefix: string = 'http://localhost:31415/rest/ng-json-web-token';

    constructor(
        private httpClient: HttpClient,
    ) { }

    async login(): Promise<void> {
        const url = `${this._urlPrefix}/login`;
        const body = {
            id: 'test',
            pwd: 'test123',
        };
        try {
            const result = await this.httpClient.post<any>(url, body).toPromise();
            this._jwtToken = result.jwtToken;
        } catch (error) {
            console.log(error);
        }
    }

    async doAnyThing(): Promise<void> {
        const url = `${this._urlPrefix}`;
        try {
            const result = await this.httpClient.get<any>(url, {
                headers: {
                    Authorization: `Bearer ${this.jwtToken}`,
                }
            }).toPromise();
        } catch (error) {
            console.error(error);
        }
    }

    async logout(): Promise<void> {
        const url = `${this._urlPrefix}/logout`;
        const options: object = {
            headers: this.jwtToken ? { Authorization: `Bearer ${this.jwtToken}` } : null
        };
        try {
            await this.httpClient.post<any>(url, {}, options).toPromise();
            this._jwtToken = '';
        } catch (error) {
            console.log(error);
        }
    }

    public get jwtToken(): string {
        return this._jwtToken;
    }

}
